$(function () {
    $("#formulario-databyte").validate({
        rules: {
            nombres: {
                required: true,
                minlength: 2,
            },
            apellidos: {
                required: true,
                minlength: 2
            },
            direccion: {
                required: true,
                minlength: 2
            },
            region: {
                required: true
            },
            comuna: {
                required: true
            },
            celular: {
                required: true,
                minlength: 9,
            },
            email: {
                required: true,
                email: true
            },
            mensaje: {
                required: true,
                minlength: 10,
            }
        },
        messages: {
            nombres: {
                required: 'Ingrese su Nombre.',
                minlength: 'Largo de nombre insuficiente - Minimo 2 caracteres.'
            },
            apellidos: {
                required: 'Ingrese sus Apellidos.',
                minlength: 'Largo de apellidos insuficiente - Minimo 2 caracteres.'
            },
            direccion: {
                required: 'Ingrese su Direccion.',
                minlength: 'Largo de direccion insuficiente - Minimo 2 caracteres.'
            },
            region: {
                required: 'Debe Escoger una Region del Listado.',
            },
            comuna: {
                required: 'Debe Escoger una Comuna del Listado.',
            },
            celular: {
                required: 'Ingrese su Numero de Celular.',
                minlength: 'Largo de Numero Celular Insuficiente - Minimo 9 digitos.'
            },
            email: {
                required: 'Ingrese su correo electrónico.',
                email: 'Formato de correo no válido. Debe tener @ y una extension. Ejemplo: @.com'
            },
            mensaje: {
                required: 'Ingrese Un Mensaje',
                minlength: 'Largo de Mensaje Insuficiente - Minimo 10 caracteres.'
            }
        }
    });
});